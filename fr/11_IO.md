# Les entrées sorties

En Ruby, les entrées sorties sont des objets de type `IO`. Il y a les `IO` classiques comme `STDIN` (entrée standard) `STDOUT` (sortie standard) ou les pipes.
Il y a les Fichiers de la classe `File` ou les sockets comme `TCPSocket`.

Ces entrées sorties reconnaissent un ensemble de méthodes :
* `<<` : Insère l'élément de droite dans l'`IO`
* `binmode` : Convertit l'`IO` en mode binaire.
* `binmode?` : Indique si l'`IO` est en mode binaire.
* `close` : Ferme l'`IO`.
* `close_read` : Ferme la lecture de l'`IO`.
* `close_write` : Ferme l'écriture de l'`IO`.
* `closed?` : Indique si l'`IO` est fermé.
* `each` / `each_line` : Lis toutes les lignes de l'`IO` et les passe dans le bloc.
* `each_byte` : Lit chaque octet de l'`IO` et les passe dans le bloc sous forme d'entiers.
* `each_char` : Lit chaque caractère de l'`IO` et les passe dans le bloc sous forme de chaîne.
* `each_codepoint` : Lit chaque caractère de l'`IO` et les passe dans le bloc sous forme d'entiers.
* `eof?` : Indique si l'`IO` est arrivé à la fin de son contenu.
* `external_encoding` : Indique l'encodage du contenu de l'`IO` si précisé. (Retourne `nil` sinon.)
* `fileno` : Renvoie le file descriptor.
* `flush` : Envoie le buffer de l'`IO` au système d'exploitation pour qu'il soit traité.
* `getbyte` : Lit un octet de l'`IO` sous forme d'entier.
* `getc` : Lit un caractère de l'`IO` sous forme de chaîne.
* `gets` : Lit une ligne de l'`IO`.
* `internal_encoding` : Indique l'encodage des chaînes Ruby si précisé. (Renvoie `nil` sinon).
* `lineno` : Indique le numéro de ligne (en partant de 0) d'un `IO` en lecture.
* `pos` : Indique l'offset (en octets) de l'`IO` par rapport à son contenu.
* `print` : Écrit l'objet donné en paramètre dans l'`IO`.
* `printf` : Écrit dans l'`IO` de la même manière qu'on utilise `format`.
* `putc` : Ecrit un caractère dans l'`IO`.
* `puts` : Ecrit une ligne dans l'`IO`.
* `read` : Lit l'`IO`.
* `read_nonblock` : Lit dans l'`IO` sans bloquer le programme si ce n'est pas possible de lire.
* `readbyte` : Lit un octet dans l'`IO`.
* `readchar` : Lit un caractère dans l'`IO`.
* `readline` : Lit une ligne dans l'`IO`.
* `readlines` : Lit toutes les lignes de l'`IO`.
* `readpartial` : Lit au mieux le nombre d'octets indiqués.
* `rewind` : remet l'IO au début.
* `seek` : Permet de placer l'`IO` à la position demandée.
* `set_encoding` : Permet de changer l'encodage de l'`IO`.
* `stat` : Renvoie un `File::Stat` décrivant l'`IO`.
* `sync` : Indique si l'`IO` est automatiquement `flush` ou non quand du contenu est ajouté.
* `tty?` : Indique si l'`IO` est un terminal ou non.
* `write` : Ecrit dans l'`IO`.
* `write_nonblock` : Ecrit dans l'`IO` sans bloquer si l'`IO` a besoin d'une synchronisation en écriture.

## Les fichiers

Les fichiers sont des `IO` qui contiennent des données sur le disque. Leur nom vous permet de les ouvrir et de réaliser diverses opérations.

### Ouvrir un fichier

Pour ouvrir un fichier vous utiliserez la méthode `open`. Son retour sera le fichier ouvert.

Vous pouvez utiliser un bloc pour que le code à l'intérieur du bloc s'exécute et que le fichier soit fermé une fois l'exécution du bloc terminé.

Exemples :
```ruby
file = open(nom, mode)
# Do thing with file
file.close

open(nom, mode) do |file|
  # Do thing with file
end
```

Les choses que vous pourrez faire seront tout ce qui est disponible dans la classe IO selon le `mode` d'ouverture.

Le `mode` est une chaîne et peut être l'une des choses suivantes :
* `r` lire uniquement, commencer au début du fichier.
* `r+'` lire et écrire, commencer au début (n'efface pas le contenu).
* `w` écrire uniquement et effacer les données qui existaient déjà.
* `w+` écrire et lire, efface les données qui existaient déjà.
* `a` écrire à la fin du fichier. (Crée le fichier s'il n'existe pas)
* `a+` écrire à la fin du fichier et pouvoir le lire. (Crée le fichier s'il n'existe pas.)

La lettre du mode peut s'accompagner de :
* `b` pour ouvrir en mode binaire.
* `t` pour ouvrir en mode texte.

Après les caractères du mode il est possible de préciser l'encodage du fichier en ajoutant `:<nom_encodage>` où `<nom_encodage>` est l'encodage du fichier.

Pour automatiquement supprimer le BOM pour les fichiers UTF ajoutez `BOM|` avant le nom de l'encodage.

Exemple : 
```ruby
File.open('test.txt', 'r:BOM|UTF-8') do |f| 
  # do some reading of f
end
```

L'encodage interne peut également être précisé à l'aide d'un autre `:<nom_encodage>` ajouté après le précédent dans le mode.

## Lire une partie ou l'intégralité d'un fichier

Si votre but n'est pas de réaliser plusieurs opérations de lecture sur un fichier, vous pouvez utiliser une fonction qui vous permet de lire tout le fichier ou un fragment sans créer d'`IO` pour ça :
* `File.read` : Similaire à un mode `'r'` mais renvoyant la plupart du temps une chaîne en ASCII-8BIT.
* `File.binread` : Similaire à un mode `'rb'`.

Le premier paramètre de ces fonctions est le nom du fichier, les autres paramètres optionnels sont la taille à lire et l'offset.

Exemples : 
```ruby
File.read('test.txt') # Lit tout test.txt et renvoie son contenu dans une chaîne.
File.read('test.txt', 50) # Lit les 50 premiers caractères de test.txt et les renvoie dans une chaîne
File.read('test.txt', 50, 2) # Lit les 50 caractères à partir du 3ème caractère dans test.txt et les renvoie dans une chaîne
```

## Ecrire une partie ou l'intégralité d'un fichier

Vous pouvez aussi vous passer de créer un `IO` pour écrire dans un fichier grâce aux méthodes suivantes :
* `File.write` : Ecrire avec un mode équivalent à `'w'`.
* `File.binwrite` : Ecrire avec un mode équivalent à `'wb'`

Les paramètres sont :
* Le nom du fichier
* Le contenu à écrire
* L'offset du contenu (optionnel, si précisé, ne remplace pas le contenu du fichier avec le contenu à écrire).

Exemple :
```ruby
File.write('test.txt', 'Hello World!')
# test.txt contiendra : Hello World!
File.write('test.txt', 'Home!', 6)
# test.txt contiendra : Hello Home!!
```

## Copier un fichier

Il existe une fonction qui permet de copier un fichier : `File.copy_stream`.

Ses paramètres sont :
* Le nom du fichier source.
* Le nom du fichier destination.
* La taille à copier (optionnel)
* L'offset dans le fichier source (optionnel)

Exemple :
```ruby
# Copier tout test.txt dans test-copie.txt :
File.copy_stream('test.txt', 'test-copie.txt')
# Copier qu'une partie de test.txt dans test-tronque.txt :
File.copy_stream('test.txt', 'test-tronque.txt', 512)
# Copier qu'un morceau de test.text :
File.copy_stream('test.txt', 'test-part.txt', 512, 1024)
```

## Renommer un fichier

Il est possible de simplement renommer un fichier à l'aide de `File.rename`.

Le premier paramètre est l'ancien nom du fichier, le second paramètre est le nouveau nom du fichier.

## Lire la taille d'un fichier

Pour lire la taille d'un fichier, il suffit d'utiliser `File.size` avec pour paramètre le nom du fichier. Quand vous avez ouvert un fichier, il y a une méthode `size` qui ne prend pas de paramètre et qui renvoie la taille du fichier ouvert.

## Ouvrir un pipe

Pour ceux qui se demandent ce que sont les pipes, c'est des flux d'E/S entre deux programmes, généralement un programme parent et un programme enfant. En Ruby on peut ouvrir un pipe à l'aide de `IO.popen`.

Exemple : 
```ruby
IO.popen('date', 'r+') { |date| puts "Date : #{date.readline}" }
```

Dans cet exemple, j'ouvre un pipe avec le programme "date", je me contente seulement de lire la date affichée en première ligne généralement.