# La gestion des erreurs

En Ruby, il y a un système de gestion des erreurs, ceci vous permet de debugguer votre programme assez simplement et ça vous permet également de gérer/d'éviter l'exécution de certains parties du code si certaines choses ne sont pas correctes.

Dans la mesure du possible les erreurs doivent être critiques. Évitez de lever des erreurs lorsque vous auriez pu être capable de réaliser des conversions (paramètres de fonction).

## Lancer une exception

Pour lancer une exception, vous avez la fonction `raise` qui permet de dire à Ruby d'arrêter l'exécution du code actuel et de passer l'exception désignée dans le système d'exception.

Il existe trois manière d'appeler `raise` :
* `raise "Mon message d'erreur"` : Quand vous n'avez pas de classe d'erreur spécifique, ça lèvera une RuntimeError avec pour message "Mon message d'erreur".
* `raise ErrorClass, "Mon message d'erreur"` : Quand vous avez une classe d'erreur spécifique à utiliser (ici ErrorClass) ça lèvera une ErrorClass avec pour message "Mon message d'erreur".
* `raise` : A utiliser quand vous avez récupéré une exception (à l'aide de `rescue`) et que vous voulez renvoyer l'exception aux `rescue` plus en amont dans la pile d'appel. 

## Attraper une exception

Pour attraper une exception vous allez utiliser le mot clé `rescue` suivi d'une expression permettant de décrire le type d'erreur que vous allez attraper. Vous pouvez mettre plusieurs `rescue` à la suite (pour différentes gestions).

Généralement le mot clé `rescue` s'utilise dans un `begin` --> `end` ou un `def` --> `end`.

Exemple :
```ruby
def ma_methode(value)
  value * 5
rescue NoMethodError
  puts "Le paramètre value (#{value}) est incorrect, il ne supporte pas la méthode `*`"
  return 0 # On retourne une valeur pour ne pas provoquer plus d'erreurs
rescue StandardError
  puts "Le paramètre value (#{value}) fait quelque chose de pas attendu avec la méthode `*`. Vous avez sûrement un problème critique dans votre programme. Arrêt..."
  raise
end

def *(v)
  raise "Bad thing"
end

ma_methode(5) # 25
ma_methode(:a) # 0
# Affiche :
# Le paramètre value (a) est incorrect, il ne supporte pas la méthode `*`
ma_methode(self)
# Affiche :
# Le paramètre value (main) fait quelque chose de pas attendu avec la méthode `*`. Vous avez sûrement un problème critique dans votre programme. Arrêt...
# Traceback (most recent call last):
#        4: from E:/Ruby25/bin/irb.cmd:19:in `<main>'
#        3: from (irb):lineno
#        2: from (irb):lineno:in `ma_methode'
#        1: from (irb):lineno:in `*'
#RuntimeError (Bad thing)
```

## Réexécuter le code après avoir corrigé le problème d'une exception

Quand vous attrapez une exception et que vous êtes capable de corriger le souci (fichier non généré par exemple) vous aimeriez bien relancer le code plutôt que vous arrêter là ou de sortir de la méthode. Pour cela, il existe un mot clé en Ruby : `retry`. Ce mot clé permet de réexécuter le code à partir du `begin` ou du `def` d'où `rescue` a été défini. Retry ne réinitialise pas le contexte donc toute modification de variable locale dans le `rescue` sera conservée.

Exemple : 
```ruby
def lire_collision_map(id_map)
  nom_fichier = format('Data/Map%<id>03d.collision.rxdata', id: id_map)
  return charger_fichier(nom_fichier)
rescue Errno::ENOENT
  puts "Le fichier #{nom_fichier} n'existe pas..."
  regenerer_collisions(id_map)
  retry
end
```
Ici j'ai utilisé des méthodes bateau parce que nous n'avons pas vu la gestion des fichiers. L'erreur `Errno::ENOENT` est l'erreur qui est généralement levée lorsqu'un fichier n'existe pas. 

## Exécuter un code quoi qu'il arrive

Dans les méthodes vous pouvez utiliser le mot clé `ensure` qui vous permet d'exécuter un code quelque soit le problème rencontré à la fin de l'exécution de la méthode.

Cette partie est appelée dans deux cas :
* `return` a été rencontré ou la méthode est arrivée normalement à son terme.
* La capture d'exception a terminé son travail.

Exemple :
```ruby
def ma_methode
  # code qui bug
rescue StandardError
  puts 'Une exception est survenue'
  raise
ensure
  puts 'Je fais le ménage'
end

ma_methode
# Affiche :
# Une exception est survenue
# Je fais le ménage
# <LogErreurRuby>
```

Ceci peut être très pratique lorsque vous réalisez des systèmes en ligne ou des choses qui nécessitent de faire un peu de ménage quoi qu'il arrive (fermeture de fichier etc...)

## Liste des différents types d'erreur

Le principale : `StandardError`, ça attrapera toute les erreurs suivantes :
* `ArgumentError` : Erreur des paramètres de méthode ou lambda.
    * `UncaughtThrowError` : Erreur qui survient quand vous utilisez `throw` avec un tag qui n'a pas de `catch`.
* `EncodingError` : Erreur qui survient lorsque vous manipulez des chaînes ou des expressions régulières d'encodage différents. Si vos données viennent d'un fichier désérialisé (Marshal) l'encodage sera potentiellement ASCII-8Bit qui est compatible avec rien, faites un `str.force_encoding('UTF-8')` pour être sur que votre chaîne qui était en UTF-8 le soit.
* `FiberError` : Quand vous manipulez un `Fiber` sans faire attention (appeler `resume` quand le `Fiber` est mort par exemple).
* `IOError` : Erreur survenant lorsque vous manipulez un fichier qui ne peut pas être manipulé comme vous le désirez (fichier fermé, ouvert dans le mauvais mode etc...)
    * `EOFError` : Quand vous êtes arrivé à la fin d'un fichier et que vous essayez de le lire.
* `IndexError` : Quand vous lisez un tableau à l'aide de `fetch` mais que vous lisez en dehors du tableau.
    * `KeyError` : Quand vous lisez un tableau associatif à l'aide de `fetch` mais que vous utilisez une clé qui n'est pas définie dans le tableau associatif.
    * `StopIteration` : Quand vous avez atteint la dernière itération d'un Enumerator (en appelant `next`) ou à utiliser pour stopper une boucle `loop`.
*  `LocalJumpError` : Cette erreur survient quand vous utilisez `yield` et qu'aucun bloc n'a été donné ou que vous utilisez `return` dans un bloc qui n'est pas exécuté dans le contexte d'une méthode.
* `NameError` : Cette erreur survient lorsque vous essayez d'accéder à quelque chose qui n'est pas défini (Constante, méthode, variable locale etc...).
    * `NoMethodError` : Cette erreur survient lorsque vous appelez explicitement une méthode d'un objet qui n'a pas de définition de cette méthode.
* `RangeError` : Cette erreur survient lorsque des fonctions native essaient de convertir des nombres dans un format supporté par du C mais que ces nombres sont trop grands.
    * `FloatDomainError` : Cette erreur survient lorsque vous manipulez `Float::INFINITY` ou `Float::NAN` dans certains types d'opérations.
* `RegexpError` : Cette erreur survient lorsque votre Regexp est mal formaté.
* `RuntimeError` : Cette erreur est une erreur utilisateur qui contient les informations nécessaires dans le message.
    * `FrozenError` : Cette erreur survient lorsque vous essayez de modifier un objet figé.
* `SystemCallError` sont toute les erreurs de type `Errno::`. Liste des constantes de `Errno` avec leur descriptions ici : [ERRNO](http://manpagesfr.free.fr/man/man3/errno.3.html)
* `ThreadError` : Cette erreur survient quand une opération invalide est réalisée sur un Thread.
* `TypeError` : Cette erreur survient lorsque le type d'un paramètre de fonction est invalide. (Souvent vérifié par les fonctions native).
* `ZeroDivisionError` : Cette erreur survient lorsque vous réalisez une division par 0 (division ou modulo).


Il existe également d'autres types d'erreurs qui elles ne sont pas attrapés par StandardError :
* `NoMemoryError` : Quand Ruby n'a pas été en mesure d'allouer de la mémoire pour quelque chose.
* `ScriptError` : Quand une erreur de script a été rencontrée.
    * `LoadError` : Quand un script ou une extension n'a pas pu être chargé depuis `require`.
    * `NotImplementedError` : Quand vous appelez des méthodes spécifique aux plateforme (`fork` par exemple) et que celle-ci ne sont pas implémentés sur votre plateforme.
    * `SyntaxError` : Quand la syntaxe du script chargé ou exécuté (`require`, `eval`) est invalide.
* `SecurityError` : Quand vous réalisez des opérations incompatible avec le niveau `$SAFE` actuel.
* `SignalException` : Quand Ruby a reçu un signal par la commande `kill`.
    * `Interrupt` : Quand l'utilisateur a utilisé CTRL+C dans le terminal.
* `SystemExit` : Quand la méthode `exit` a été appelée.
* `SystemStackError` : Quand vous avez fait une méthode récursive qui s'est trop appelée ou que vous avez fait du mauvais mokey patch.