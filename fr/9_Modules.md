# Les modules
En Ruby les modules sont des sortes d'espace de nom mais permettent également de réaliser ce qu'on appelle les Mixin.

## Définir un module

Pour définir un module, c'est assez simple, nous utilisons le mot clé `module` et nous le faisons suivre d'un nom de constante comme pour les classes.

Exemple :
```ruby
module MyModule

end
```

## Le contenu d'un module

Un module peut contenir :
* Des classes (espace de noms)
* Des modules (espace de noms)
* Des constantes (espace de noms)
* Des méthodes (Mixin)
* Des méthodes de module (Mixin + espace de nom)

Le parfait exemple est le module `Math`. `Math::PI` est la constante pi, on peut également accéder la fonction cosinus par `Math.cos`. 

## Inclure un module

Si vous désirez accéder à la fonction cosinus dans le code de votre classe nous allons utiliser la méthode `include` de votre classe pour inclure les fonctions du module à l'intérieur. Ceci nous évite d'écrire `Math.cos`.

Exemple :
```ruby
class MaClass
  include Math

  # Function using cos
  # @return [Float]
  def use_cos
    cos(PI / 2)
  end
end
```

Comme vous pouvez le voir, on a également accès aux constantes.

## Définir des méthodes dans un module

Quand vous définissez des méthodes dans un module, il y a deux cas de figure qui se présentent :
* Vous allez seulement utiliser ces méthodes par Mixin, dans ce cas vous ne donnez pas d'information spécifique sur ces méthodes.
* Vous allez utiliser ces méthodes aussi bien avec des Mixin qu'en accès direct (exemple de `Math.cos`), pour cela vous allez utiliser le mot clé `module_function` de la même manière que les mots clés `private` et `public`.

Exemple :
```ruby
# My module
module MyModule
  # Method that does something (only visible when included)
  def do_something
  end

  # Method that can be called like this : MyModule.method_name
  module_function def method_name
  end

  # All the following method will be module_function
  module_function
  
  # Other method that can be called like this : MyModule.other_method_name
  def other_method_name
  end
end
```

Notez que les `module_function` seront privées une fois incluses alors que sans détails supplémentaires les méthodes classiques seront publiques une fois incluses.

## Le mécanisme extend

En Ruby il existe une fonction qui permet d'inclure des méthodes d'une module dans une classe, un module ou un objet comme des méthodes singleton, cette méthode est `extend`.

Les méthodes copiées du module via `extend` porteront les mêmes attributs que dans le module en question.

Exemple :
```ruby
# My class
class MyClass
  extend MyModule
end
```
On pourra être en mesure d'appeler `MyClass.do_something`, par contre les méthodes `method_name` et `other_method_name` sont privées du coup elles ne pourront être appelées que par des méthodes singleton de MyClass.

## L'exploration de constante

Comme on utilise les modules comme espace de noms, il arrive qu'on tombe sur le cas où des constantes portent le même nom dans plusieurs modules :
* `A::MyClass`
* `B::MyClass`
* `MyClass`

Si vous voulez dans le module B créer une classe qui hérite de `MyClass` du contexte global vous êtes bien embêtés car `MyClass` dans le module `B` pointe vers `B::MyClass`. 
Pour cela Ruby a prévu un système assez simple : Vous pouvez voir le `::` comme un `/` des systèmes UNIX. Autrement dit, si vous voulez accéder au `MyClass` du contexte global depuis le module `B` il vous suffira d'écrire `::MyClass`.

Pour accéder au `MyClass` du module `A` depuis le module `B` vous avez deux solutions :
* `A::MyClass` : S'il n'y a pas de constante A définie dans le module `B`, Ruby ira faire un petit tour dans la class `Object` pour voir si elle n'existe pas avant de lancer une erreur.
* `::A::MyClass` : Vous dites explicitement ce que vous voulez. Ce n'est pas nécessairement plus optimisé mais au moins vous ne risquez pas de charger la mauvaise classe.

Exemple :
```ruby
class MyClass
end
module A
  class MyClass
  end
end
module B
  class MyClass
  end

  p MyClass # Affiche : B::MyClass
  p ::MyClass # Affiche : MyClass
  p A::MyClass # Affiche : A::MyClass
  p ::A::MyClass # Affiche : A::MyClass
end
```

## Mot de la fin
Avec tout ça, je vous ai donné ce qu'il faut pour bien définir les choses en Ruby, il nous reste plus que la gestion des erreurs et quelques petites informations.