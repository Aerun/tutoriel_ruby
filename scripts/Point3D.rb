# Make sure we have Point2D
require_relative 'Point2D'

# Class describing a 3D point
class Point3D < Point2D
  # @return [Integer] z coordinate of the point
  attr_accessor :z

  # @overload new(x_pos, y_pos, z_pos)
  #   Create a new point by giving its coordinates
  #   @param x_pos [Integer] x coordinate of the point
  #   @param y_pos [Integer] y coordinate of the point
  #   @param z_pos [Integer] z coordinate of the point
  # @overload new(point)
  #   Create a new point by copying another point
  #   @param point [Point3D, Point2D] the other point
  def initialize(x_pos, y_pos = 0, z_pos = 0)
    set_coordinates(x_pos, y_pos, z_pos)
  end

  # Define the new z coordinate of the point
  # @param value [Integer] new z coordinate
  def z=(value)
    @z = safe_to_i(value)
  end

  # @overload set_coordinates(x_pos, y_pos, z_pos)
  #   Set the new coordinate of the point
  #   @param x_pos [Integer] x coordinate of the point
  #   @param y_pos [Integer] y coordinate of the point
  #   @param z_pos [Integer] z coordinate of the point
  #   @return [self]
  # @overload set_coordinates(point)
  #   Set the new coordinate of the point by copying another point
  #   @param point [Point3D, Point2D] the other point
  #   @return [self]
  def set_coordinates(x_pos, y_pos = 0, z_pos = 0)
    return set_coordinates(x_pos.x, x_pos.y, x_pos.z) if x_pos.is_a?(Point3D)
    return set_coordinates(x_pos.x, x_pos.y, 0) if x_pos.is_a?(Point2D)
    self.x = x_pos
    self.y = y_pos
    self.z = z_pos
    return self
  end

  # @overload move(delta_x, delta_y, delta_z)
  #   Move the point by a certain amount of delta_x, a certain amount of delta_y and a certain amount of delta_z
  #   @param delta_x [Integer] amount of x we want to move the point
  #   @param delta_y [Integer] amout of y we want to move the point
  #   @param delta_z [Integer] amout of z we want to move the point
  #   @return [self]
  # @overload move(delta_point)
  #   Move the point by a certain amount of delta_point coordinates
  #   @param delta_point [Point3D, Point2D]
  #   @return [self]
  def move(delta_x, delta_y = 0, delta_z = 0)
    return move(delta_x.x, delta_x.y, delta_x.z) if delta_x.is_a?(Point3D)
    return super(delta_x.x, delta_x.y) if delta_x.is_a?(Point2D)
    @x += safe_to_i(delta_x)
    @y += safe_to_i(delta_y)
    @z += safe_to_i(delta_z)
    return self
  end

  # Calculate the square distance of the point to another point
  # @param other_point [Point3D, Point2D] the other point we want to calculate the square distance
  # @return [Integer] the square distance
  private def distance2(other_point)
    return super + @z**2 if other_point.is_a?(Point2D)
    return super + (@z - other_point.z)**2
  end

  # Calculate the distance of the point to another point
  # @param other_point [Point3D, Point2D]
  # @return [Integer] the distance
  def distance(other_point)
    return (distance2(other_point)**0.5).round
  end
end